# This file is a step-by-step example of test driven development (TDD)
#   It shows the development of a function which pulls the first digit of a floating point number representing a
#     property price (in $100ks) for the detection of fraud.
# For a (spoiler-free) walk through the process of writing the benford function in a TDD fashion, see the presentation
#   or navigate to https://gitlab.com/TapdancingRodent/pttdd/-/commits/feat/tdd-benford/test_driven_benford.py
