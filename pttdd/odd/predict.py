import logging

import numpy as np
import tensorflow as tf

logging.basicConfig(level=logging.INFO)

DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
LOGGER = logging.getLogger(__name__)


def load_model(model_file="trained_model.h5"):
    return tf.keras.load(model_file)


def predict_digits(model, digit_sequence):
    digit_label_probabilities = model.predict(digit_sequence)
    digits = [
        DIGITS[np.argmax(label_proba)]
        for label_proba in digit_label_probabilities
        if np.argmax(label_proba) != 10  # Drop random classifications
    ]

    if 0 in digits:
        logging.WARN("Detected zero in output!!!")
        return []

    else:
        return digits
