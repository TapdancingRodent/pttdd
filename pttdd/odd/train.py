import os

import numpy as np
import pandas as pd
import tensorflow as tf


def build_simple_model() -> tf.keras.Model:
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(32, [3, 3], activation="relu", input_shape=(28, 28, 1)))
    model.add(tf.keras.layers.Conv2D(64, [3, 3], activation="relu"))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Dropout(0.25))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(128, activation="relu"))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(10))

    model.compile(
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(),
        metrics=["accuracy"],
    )

    return model


def train_model(data_folder: str) -> tf.keras.Model:
    non_zeros_df = pd.read_csv(f"{data_folder}/train_non_zeros.csv")

    tt_msk = np.random.rand(len(non_zeros_df)) < 0.8
    train_df = non_zeros_df[tt_msk]
    test_df = non_zeros_df[~tt_msk]

    # Implicitly checks that each row has 28*28+1=785 values
    x_train = train_df.drop(columns=["label"]).values.reshape((len(train_df), 28, 28, 1)) / 255
    y_train = train_df["label"].values

    # I don't hugely trust reshape so I just want to sense check that the first row came out OK
    first_train_digit_values = train_df.drop(columns=["label"]).iloc[0, :].values / 255
    assert np.linalg.norm(x_train[0, :, :].flatten(), 0) == np.linalg.norm(first_train_digit_values.flatten(), 0)
    assert np.linalg.norm(x_train[0, :, :].flatten(), 1) == np.linalg.norm(first_train_digit_values.flatten(), 1)
    assert np.linalg.norm(x_train[0, :, :].flatten(), 2) == np.linalg.norm(first_train_digit_values.flatten(), 2)
    assert len(y_train) == len(train_df)

    x_test = test_df.drop(columns=["label"]).values.reshape((len(test_df), 28, 28, 1)) / 255
    y_test = test_df["label"].values

    num_train_noise_samples = x_train.shape[0] // 10  # We want roughly balanced classes
    noise_x_train = np.random.choice(x_train.flatten(), 28 * 28 * num_train_noise_samples).reshape(
        (num_train_noise_samples, 28, 28, 1)
    )
    noise_y_train = np.ones((num_train_noise_samples,)) * 10

    # More reshape suspicion
    assert x_train.shape[1:] == noise_x_train.shape[1:]

    num_test_noise_samples = x_test.shape[0] // 10  # We want roughly balanced classes
    noise_x_test = np.random.choice(x_test.flatten(), 28 * 28 * num_test_noise_samples).reshape(
        (num_test_noise_samples, 28, 28, 1)
    )
    noise_y_test = np.ones((num_test_noise_samples,)) * 10

    lowest_max_val = noise_x_train.max(axis=(1, 2)).min()
    highest_min_val = noise_x_train.min(axis=(1, 2)).max()
    print(f"Smallest maximum was {lowest_max_val} and largest minimum was {highest_min_val} - this doesn't suck")

    assert lowest_max_val > 0.99
    assert highest_min_val < 0.001

    model = build_simple_model(num_classes=11)

    zeros_df = pd.read_csv(f"{data_folder}/train_zeros_TO_BE_DELETED.csv")
    os.remove(f"{data_folder}/train_zeros_TO_BE_DELETED.csv")

    tt_msk = np.random.rand(len(zeros_df)) < 0.8
    train_zeros_df = zeros_df[tt_msk]
    test_zeros_df = zeros_df[~tt_msk]

    # Implicitly checks that each row has 28*28+1=785 values
    x_train_zeros = train_zeros_df.drop(columns=["label"]).values.reshape((len(train_zeros_df), 28, 28, 1)) / 255
    y_train_zeros = train_zeros_df["label"].values

    x_test_zeros = test_zeros_df.drop(columns=["label"]).values.reshape((len(test_zeros_df), 28, 28, 1)) / 255
    y_test_zeros = test_zeros_df["label"].values

    x_combined_train = np.vstack([x_train, noise_x_train, x_train_zeros])
    y_combined_train = np.hstack([y_train, noise_y_train, y_train_zeros])
    x_combined_test = np.vstack([x_test, noise_x_test, x_test_zeros])
    y_combined_test = np.hstack([y_test, noise_y_test, y_test_zeros])

    model.fit(
        x_combined_train,
        y_combined_train,
        batch_size=128,
        epochs=1,
        verbose=1,
        validation_data=(x_combined_test, y_combined_test),
    )

    print(model.evaluate(x_combined_test, y_combined_test))


if __name__ == "__main__":
    model = train_model("data")
    model.save("trained_model.h5")
