import os

import numpy as np
import tensorflow as tf

from pttdd.pttdd.data import generate_noise_samples, load_split_format_data


def build_simple_model(num_classes=10) -> tf.keras.Model:
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv2D(32, [3, 3], activation="relu", input_shape=(28, 28, 1)))
    model.add(tf.keras.layers.Conv2D(64, [3, 3], activation="relu"))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(tf.keras.layers.Dropout(0.25))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(128, activation="relu"))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(num_classes))

    model.compile(
        loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
        optimizer=tf.keras.optimizers.Adam(),
        metrics=["accuracy"],
    )

    return model


def train_model(data_folder: str) -> tf.keras.Model:
    # I tested
    #  - A super skinny version of training
    #    - Pointed at a test folder with minimal data of the right shape
    #  - That zeros data is deleted

    (x_train, y_train), (x_test, y_test) = load_split_format_data(f"{data_folder}/train_non_zeros.csv")

    noise_x_train, noise_y_train = generate_noise_samples(x_train)
    noise_x_test, noise_y_test = generate_noise_samples(x_test)

    model = build_simple_model(num_classes=11)

    (x_train_zeros, y_train_zeros), (x_test_zeros, y_test_zeros) = load_split_format_data(
        f"{data_folder}/train_zeros_TO_BE_DELETED.csv"
    )
    os.remove(f"{data_folder}/train_zeros_TO_BE_DELETED.csv")

    x_combined_train = np.vstack([x_train, noise_x_train, x_train_zeros])
    y_combined_train = np.hstack([y_train, noise_y_train, y_train_zeros])
    x_combined_test = np.vstack([x_test, noise_x_test, x_test_zeros])
    y_combined_test = np.hstack([y_test, noise_y_test, y_test_zeros])

    model.fit(
        x_combined_train,
        y_combined_train,
        batch_size=128,
        epochs=1,
        verbose=1,
        validation_data=(x_combined_test, y_combined_test),
    )

    print(model.evaluate(x_combined_test, y_combined_test))
    return model
