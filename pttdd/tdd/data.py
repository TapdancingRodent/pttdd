import numpy as np
import pandas as pd


def _split_data(data_df, train_proportion):
    # I tested
    #  - that the proportions come out OK
    #  - that the tiny dataframes don't fail
    #  - that the results don't overlap

    # I was too lazy to test
    #  - that the outputs have roughly balanced classes

    if len(data_df) < 2:
        raise ValueError("Cannot split data of insufficient (at least 2 entries) quantity")

    tt_msk = np.random.rand(len(data_df)) < train_proportion
    return data_df[tt_msk], data_df[~tt_msk]


def _format_data(data_df):
    # I tested
    #  - that bad data threw exceptions
    #  - that when reshaping, the first row contains the same data
    #  - that the result was scaled onto [0, 1]

    X = data_df.drop(columns=["label"]).values.reshape((len(data_df), 28, 28, 1)) / 255
    y = data_df["label"].values
    return X, y


def load_split_format_data(data_path: str, train_proportion: float = 0.8):
    # I  tested nothing - this just calls other functionality

    data_df = pd.read_csv(data_path)

    train_df, test_df = _split_data(data_df, train_proportion)

    return _format_data(train_df), _format_data(test_df)
