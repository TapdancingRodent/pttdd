import logging
from typing import Sequence

import numpy as np
from pydantic import BaseModel, validator

logging.basicConfig(level=logging.INFO)

DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
LOGGER = logging.getLogger(__name__)


class DetectedZeroError(ValueError):
    pass


class DigitData(BaseModel):
    # I tested
    #  - That garbage data throws errors
    #  - That I can convert pixels from the input into numpy arrays

    pixel_data: Sequence[Sequence[float]]

    @validator("pixel_data")
    def validate_pixels_shape(cls, val):
        assert len(val) == 28
        for row in val:
            assert len(row) == 28
        return val

    @property
    def pixels(self):
        return np.array(self.pixel_data)[:, :, np.newaxis]  # The ML model takes an extra empty dimension


def _parse_label_proba(digit_label_proba: Sequence[Sequence[float]]) -> Sequence[int]:
    # I tested
    #  - Empty sequences
    #  - Valid sequences
    #  - Sequences with noise digits
    #  - That digits outside the known classes raise errors

    return [
        DIGITS[np.argmax(label_proba)]
        for label_proba in digit_label_proba
        if np.argmax(label_proba) != 10  # Drop random classifications
    ]


def _check_for_zeros(digits: Sequence[int]):
    # I tested
    #  - Sequences with zeros, capturing logging output

    if 0 in digits:
        LOGGER.warn("Detected zero in output!!!")
        raise DetectedZeroError


def _collate_pixels(digit_input: Sequence[DigitData]):
    # I tested
    #  - Empty lists
    #  - Single item lists
    #  - Multi item lists
    #  - That the output of collating pixels looks like real data

    return np.array([digit.pixels for digit in digit_input])


def predict_digits(model, digit_input: Sequence[DigitData]):
    # I tested
    #  - That data passes through the system as expected
    #  - Classifying "canonical" digits (though I only did it for show - the test model is awful)

    digit_sequence = _collate_pixels(digit_input)
    digit_label_probabilities = model.predict(digit_sequence)
    digits = _parse_label_proba(digit_label_probabilities)
    _check_for_zeros(digits)

    return digits
