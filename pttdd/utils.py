from matplotlib import pyplot as plt


def get_pixel_data_and_label(row):
    return row.drop(columns=["label"]).values.reshape((28, 28)), row["label"], row.name


def create_plot_of_digit(pixel_matrix, label, index):
    plt.imshow(pixel_matrix, interpolation="none", cmap="gray")
    plt.title(f"index: {index}, Label: {label}")
    plt.xticks([])
    plt.yticks([])


def show_digit_rows(rows):
    for _, row in rows.iterrows():
        plt.figure()
        create_plot_of_digit(**get_pixel_data_and_label(row))
    plt.show()


def show_digit_row(row):
    create_plot_of_digit(**get_pixel_data_and_label(row))
    plt.show()
