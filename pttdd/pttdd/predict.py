import logging
from typing import Sequence

import numpy as np
import tensorflow as tf
from pydantic import BaseModel, validator

logging.basicConfig(level=logging.INFO)

DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
LOGGER = logging.getLogger(__name__)


class DigitData(BaseModel):
    # I pretended to test
    #  - That garbage data throws errors
    #  - That I can convert pixels from the input into numpy arrays

    pixel_data: Sequence[Sequence[float]]

    @validator("pixel_data")
    def validate_pixels_shape(val):
        assert len(val) == 28
        for row in val:
            assert len(row) == 28
        return val

    @property
    def pixels(self):
        return np.array(self.pixel_data)


def load_model(model_file="trained_model.h5"):
    # I pretended to test nothing - this just calls other functionality

    return tf.keras.load(model_file)


def _validate_digits(digit_label_probabilities):
    # I pretended to test
    #  - Empty sequences
    #  - Sequences with zeros, capturing logging output
    #  - Sequences with noise digits

    # I forgot to test
    #  - What we should do with digits outside known classes

    digits = [
        DIGITS[np.argmax(label_proba)]
        for label_proba in digit_label_probabilities
        if np.argmax(label_proba) != 10  # Drop random classifications
    ]

    if 0 in digits:
        LOGGER.warn("Detected zero in output!!!")
        return []

    else:
        return digits


def _collate_pixels(digit_input: Sequence[DigitData]):
    # I pretended to test
    #  - Empty lists
    #  - Single item lists
    #  - Multi item lists

    # I overlooked pretending to test
    #  - That the output of collating pixels looks like real data

    return np.array([digit.pixels for digit in digit_input])[
        :, :, :, np.newaxis
    ]  # The ML model takes an extra empty dimension


def predict_digits(model, digit_input: Sequence[DigitData]):
    # I pretended to test nothing - this just calls other functionality

    digit_sequence = _collate_pixels(digit_input)
    digit_label_probabilities = model.predict(digit_sequence)

    return _validate_digits(digit_label_probabilities)
