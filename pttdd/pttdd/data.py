import numpy as np
import pandas as pd


def _split_data(data_df, train_proportion):
    # I pretended to test
    #  - that the proportions come out OK
    #  - that the tiny dataframes don't fail

    # I overlooked pretending to test
    #  - that the outputs are independent
    #  - that the outputs have roughly balanced classes

    if len(data_df) < 2:
        raise ValueError("Cannot split data of insufficient (at least 2 entries) quantity")

    tt_msk = np.random.rand(len(data_df)) < train_proportion
    return data_df[tt_msk], data_df[~tt_msk]


def _format_data(data_df):
    # I pretended to test
    #  - that bad data threw exceptions
    #  - that when reshaping, the first row contains the same data

    # I overlooked pretending to test
    #  - that the result was scaled onto [0, 1]

    X = data_df.drop(columns=["label"]).values.reshape((len(data_df), 28, 28, 1)) / 255
    y = data_df["label"].values
    return X, y


def load_split_format_data(data_path: str, train_proportion: float = 0.8):
    # I pretended to test nothing - this just calls other functionality

    data_df = pd.read_csv(data_path)

    train_df, test_df = _split_data(data_df, train_proportion)

    return _format_data(train_df), _format_data(test_df)


def generate_noise_samples(example_data_df, noise_class=10):
    # I pretended to test
    #  - that data comes out the right shape
    #  - that tiny datasets don't fail

    num_noise_samples = max(example_data_df.shape[0] // 10, 1)  # We want roughly balanced classes
    sampled_pixels = np.random.choice(example_data_df.flatten(), 28 * 28 * num_noise_samples)
    noise_x = sampled_pixels.reshape((num_noise_samples, 28, 28, 1))
    noise_y = np.ones((num_noise_samples,)) * noise_class

    return noise_x, noise_y
