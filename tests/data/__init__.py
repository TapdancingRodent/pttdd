import csv
import shutil
from contextlib import contextmanager
from pathlib import Path

import pandas as pd
import tensorflow as tf

DATA_FOLDER = Path(__file__).parent
TRAIN_NON_ZEROS_FILE = DATA_FOLDER / "train_non_zeros.csv"
TRAIN_ZEROS_FILE = DATA_FOLDER / "train_zeros.csv"
TRAINED_MODEL_FILE = DATA_FOLDER / "trained_model.h5"
CANONICAL_ZERO_FILE = DATA_FOLDER / "canonical_zero_2d.csv"
CANONICAL_ONE_FILE = DATA_FOLDER / "canonical_one_2d.csv"
CANONICAL_NOISE_FILE = DATA_FOLDER / "canonical_noise_2d.csv"


def get_representative_training_data() -> pd.DataFrame:
    """Get a set of representative data"""
    return pd.read_csv(str(TRAIN_NON_ZEROS_FILE))


@contextmanager
def copied_zeros_data_folder() -> str:
    """Set up a test data folder with a copy of zero data which should be deleted"""
    copied_data_file = DATA_FOLDER / "train_zeros_TO_BE_DELETED.csv"
    try:
        shutil.copyfile(str(TRAIN_ZEROS_FILE), str(copied_data_file))
        yield DATA_FOLDER

    finally:
        if copied_data_file.is_file():
            copied_data_file.unlink()

        assert TRAIN_NON_ZEROS_FILE.is_file()  # Check we didn't delete other stuff


def get_trained_test_model() -> tf.keras.Model:
    """Load a dummy trained model"""
    return tf.keras.models.load_model(str(TRAINED_MODEL_FILE))


def _get_canonical_pixel_data(file_name):
    """Load examples of a canonical digits for testing"""
    with open(file_name) as pixel_file:
        pixel_reader = csv.reader(pixel_file)
        next(pixel_reader)  # Discard the header
        canonical_pixels = list(pixel_reader)

    return canonical_pixels


def get_canonical_digit_pixels():
    """Load examples of canonical digits for testing"""
    return {
        "0": _get_canonical_pixel_data(CANONICAL_ZERO_FILE),
        "1": _get_canonical_pixel_data(CANONICAL_ONE_FILE),
        "noise": _get_canonical_pixel_data(CANONICAL_NOISE_FILE),
    }
