import numpy as np
import pandas as pd
import pytest

from pttdd.tdd.data import _format_data, _split_data, load_split_format_data

from .data import TRAIN_NON_ZEROS_FILE, get_representative_training_data


def _get_test_df(entries=100):
    """Return a dataframe for testing with placeholder data"""
    return pd.DataFrame(
        {
            "label": ["one"] * entries,
            "value": [1] * entries,
        }
    )


def test_split_data_proportions():
    """
    WHEN splitting data
    THEN the proportions will come out about right
    """
    test_frame = _get_test_df()
    tr, te = _split_data(test_frame, 0.8)

    assert len(tr) > 70
    assert len(tr) < 90
    assert len(tr) + len(te) == 100

    assert len(_split_data(test_frame, 0.4)[0]) < 50
    assert len(_split_data(test_frame, 0.4)[0]) > 30


def test_split_outputs_unique():
    """
    WHEN splitting data
    THEN the returned sets will have no overlapping keys
    """
    test_frame = _get_test_df()
    tr, te = _split_data(test_frame, 0.8)

    assert not set(tr.index.values).intersection(te.index.values)


def test_split_insufficient_data():
    """
    WHEN splitting data with very numbers of small values
    THEN an error will be raised
    """
    with pytest.raises(ValueError):
        _split_data(_get_test_df(entries=0), 0.8)

    with pytest.raises(ValueError):
        _split_data(_get_test_df(entries=1), 0.8)


def test_split_representative():
    """
    WHEN splitting data representative of our training problem
    THEN nothing will go wrong
    """
    _split_data(get_representative_training_data(), 0.8)


def test_format_bad_data():
    """
    WHEN formatting bad data
    THEN an exception will be thrown
    """
    with pytest.raises(ValueError):
        _format_data(_get_test_df())

    with pytest.raises(ValueError):
        _format_data(get_representative_training_data().drop(columns=["pixel0"]))


def test_format_reshape_preserves_data():
    """
    WHEN formatting good data
    THEN the reshaped data will be the same in the first row
    """
    representative_df = get_representative_training_data()
    x_formatted, y_formatted = _format_data(representative_df)

    first_train_digit_values = representative_df.drop(columns=["label"]).iloc[0, :].values / 255
    assert np.linalg.norm(x_formatted[0, :, :].flatten(), 0) == np.linalg.norm(first_train_digit_values.flatten(), 0)
    assert np.linalg.norm(x_formatted[0, :, :].flatten(), 1) == np.linalg.norm(first_train_digit_values.flatten(), 1)
    assert np.linalg.norm(x_formatted[0, :, :].flatten(), 2) == np.linalg.norm(first_train_digit_values.flatten(), 2)
    assert len(y_formatted) == len(x_formatted)


def test_format_scale():
    """
    WHEN formatting good data
    THEN the reshaped data will be scaled onto [0, 1]
    """
    representative_df = get_representative_training_data()
    x_formatted, _ = _format_data(representative_df)

    assert x_formatted.max() == 1
    assert x_formatted.min() == 0


def test_load_representative():
    """
    WHEN loading data representative of our training problem
    THEN nothing will go wrong
    """
    load_split_format_data(str(TRAIN_NON_ZEROS_FILE))
