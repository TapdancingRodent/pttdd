from pttdd.tdd.modelling import build_simple_model, train_model

from .data import copied_zeros_data_folder


def test_training_deletes_data():
    """
    WHEN training a model
    THEN zeros will be deleted and a trained model will be returned
    """
    with copied_zeros_data_folder() as data_folder:
        assert (data_folder / "train_zeros_TO_BE_DELETED.csv").is_file()
        trained_model = train_model(str(data_folder))
        assert not (data_folder / "train_zeros_TO_BE_DELETED.csv").is_file()

    assert build_simple_model().history is None
    assert trained_model.history is not None
