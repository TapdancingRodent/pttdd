import pytest
from pydantic import ValidationError

from pttdd.tdd.predict import (
    DetectedZeroError,
    DigitData,
    _check_for_zeros,
    _collate_pixels,
    _parse_label_proba,
    predict_digits,
)

from .data import get_canonical_digit_pixels, get_trained_test_model


def _get_empty_digit():
    """Build a pixel data object containing no data"""
    return DigitData(pixel_data=[[0] * 28] * 28)


def test_zero_checking():
    """
    WHEN checking for zeros in the digits output
    THEN digits being present will cause a warning and an exception
    """
    _check_for_zeros([1, 2, 3, 7, 6])
    with pytest.warns(Warning):
        with pytest.raises(DetectedZeroError):
            _check_for_zeros([1, 2, 3, 0, 7, 6])


def test_parse_label_valid():
    """
    WHEN parsing label probabilities with valid data
    THEN there will be no errors and the correct digits will be output
    """
    assert _parse_label_proba([[0.1, 0.1, 0.1, 0.1, 0.1, 0.9, 0.1, 0.1, 0.1, 0.1, 0.1]]) == [5]
    assert (
        _parse_label_proba(
            [
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.9, 0.1, 0.1, 0.1, 0.1, 0.1],
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.3, 0.1, 0.1, 0.1, 0.1],
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1],
            ]
        )
        == [5, 6, 7]
    )


def test_parse_label_invalid():
    """
    WHEN parsing label probabilities with an extra digit
    THEN an assertion will be raised
    """
    with pytest.raises(IndexError):
        _parse_label_proba([[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.9]])


def test_parse_label_drops_noise():
    """
    WHEN parsing label probabilities with noise values
    THEN the noise digits will be omitted
    """
    assert not _parse_label_proba([[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.9]])
    assert (
        _parse_label_proba(
            [
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.9, 0.1, 0.1, 0.1, 0.1, 0.1],
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.9],
                [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.2, 0.1, 0.1, 0.1],
            ]
        )
        == [5, 7]
    )


def test_digit_pixels_good_data():
    """
    WHEN constructing a pixel data object containing valid pixel data
    THEN everything will work fine
    """
    _get_empty_digit()


def test_digit_pixels_bad_data():
    """
    WHEN constructing a pixel data object containing invalid pixel data
    THEN an error will be raised
    """
    with pytest.raises(ValidationError):
        DigitData(pixel_data=[[]])


def test_digit_pixels_extraction():
    """
    WHEN extracting pixel data from a valid pixel data object
    THEN the extracted pixels will be of the correct shape
    """
    assert _get_empty_digit().pixels.shape == (28, 28, 1)


def test_multiple_digit_pixels_collation():
    """
    WHEN collating pixel data from multiple digits
    THEN the data will be extracted without errors and of the right shape
    """
    assert _collate_pixels([]).size == 0
    single_digit_array = _collate_pixels([_get_empty_digit()])
    assert single_digit_array.shape == (1, 28, 28, 1)

    double_digit_array = _collate_pixels([_get_empty_digit(), _get_empty_digit()])
    assert double_digit_array.shape == (2, 28, 28, 1)


def test_digit_prediction():
    """
    WHEN predicting digits
    THEN the only acceptable errors are for the detection of zeros
    """
    model = get_trained_test_model()
    digit_pixel_dict = get_canonical_digit_pixels()
    digit_data = [DigitData(pixel_data=pixels) for pixels in digit_pixel_dict.values()]

    try:
        predict_digits(model, digit_data)

    except DetectedZeroError:
        pass
