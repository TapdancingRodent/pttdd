# Pretending-to-Test Driven Development

This repository contains some slightly tongue in cheek examples of how pretending to write tests can positively impact code quality.
It is intended to stand as an example of how Test Driven Development's deliberate and strucutred approach has _implicit_ quality benefits alongside the explicit benefits of a well-tested code base.
A companion presentation with a primer of Test Driven Development and Pretending-to-Test Driven Development is included (with speakers' notes) in this repository.
A practical example of how Test Driven Development is executed can be seen in the history (see [GitLab](https://gitlab.com/TapdancingRodent/pttdd/-/commits/feat/tdd-benford/test_driven_benford.py)) of the `test_driven_benford.py` code snippet.

## Repository Structure

In the source code, you will find three versions of the same functionality developed using three different methodologies which all ship as part of the `pttdd` package.
The problem brief (described below) was the same for all three implementations and some initial EDA to explore the problem can be found in the `notebooks` folder.
Each development methodology jumps off at the completion of EDA, and each involved copying in some of the prototype functionality developed in the notebook.

The three different development methodologies for comparison were:

- Optimisim Drive Development (`odd`) - a naive approach to implementing the requirements.
- Pretending-to-Test Driven Development (`pttdd`) - I imagined that I had written tests for functionality before I wrote it.
- Test Driven Development (`ttd`) - a Test Driven implementation (included for reference).

The `tdd` implementation  serves as a high water mark for code quality over and above `pttdd` and also includes some data examples (in the `tests` folder) which may be of interest, though I stopped short of drawing a "canonical zero" myself.
As much as possible, I have also gone back and annotated issues I overlooked pretending to test but later caught while developing the `tdd` approach.

## Problem Brief

The use case for the code developed here is for a digit recognition system being built for Numberist Joe. Classifying chains of hand-written digits saves Joe's team time and energy (he assures us), but he has some unusual requirements...

1. Robustly detect digits in sequences of hand-written images
2. Manage zeros independently in training data
    - "Be careful... you can't trust any number with no pointy bits", Joe says
3. During training, zeros must be loaded at the last possible second and purged right after
    - To prevent any damage the zeros might do to Joe's computer systems
4. Obviously, reject any "unclean" strings of digits (those containing a zero)
5. Record all zeros you find for later analysis
    - So that Joe can find any physical troublemakers he might have overlooked
6. The scanner sometimes accidentally reads random noise which should be ignored
    - Unfortunately Joe has no data of this so we should just do our best

## Data

The MNIST data for this work was adapted from [Kaggle](https://www.kaggle.com/c/digit-recognizer/data).
